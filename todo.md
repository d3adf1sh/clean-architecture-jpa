# To do

- Executar os bancos de dados em contêineres;
- Converter modelos através de um mapeador, se possível automatizado;
- Remover o termo **Argumento** nas validações;
- Usar `IllegalArgumentException` ao invés de `NullPointerException` para argumentos nulos;
- Testar outras formas de validação (Notifications/Either, método público, factory, etc);
- Implementar os presenters (interactor x controlador, modelo próprio, UI vs web);
- Mover os adaptadores de entrada (`rest`) para a package `recursos` ou `pontosdeentrada`;
- Usar um repositório por operação;
- Indexar a busca por CPFJ no MongoDB;
- Remover os milissegundos ao persistir data e hora;
- Implementar adaptadores para Swing e serviço externo;
- Renomear o módulo `bootstrap` para `server` ou `configuration`;
- Usar constantes para dependências no Maven;
- TDD.
