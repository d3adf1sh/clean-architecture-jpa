package arquitetura.clean.aplicacao.servicos.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import arquitetura.clean.modelo.valores.CPFJ;

import java.util.Objects;

public final class ServicoDeExclusaoDeCliente implements CasoDeUsoDeExclusaoDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeExclusaoDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public ClienteDeResposta excluir(CPFJ cpfj) throws ClienteNaoEncontrado {
        Objects.requireNonNull(cpfj, "O argumento \"cpfj\" não pode ser nulo.");
        return repositorioDeClientes.excluir(cpfj)
                .map(ClienteDeResposta::converterParaResposta)
                .orElseThrow(() -> new ClienteNaoEncontrado("Cliente %s não encontrado.".formatted(cpfj.formatar())));
    }
}
