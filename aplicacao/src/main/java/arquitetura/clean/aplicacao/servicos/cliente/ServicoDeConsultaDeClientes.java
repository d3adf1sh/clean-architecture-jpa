package arquitetura.clean.aplicacao.servicos.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeConsultaDeClientes;
import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;

import java.util.List;
import java.util.Objects;

public final class ServicoDeConsultaDeClientes implements CasoDeUsoDeConsultaDeClientes {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeConsultaDeClientes(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public List<ClienteDeResposta> consultar(String nomeFantasia) {
        Objects.requireNonNull(nomeFantasia, "O argumento \"nomeFantasia\" não pode ser nulo.");
        return repositorioDeClientes.consultar(nomeFantasia)
                .stream()
                .map(ClienteDeResposta::converterParaResposta)
                .toList();
    }
}
