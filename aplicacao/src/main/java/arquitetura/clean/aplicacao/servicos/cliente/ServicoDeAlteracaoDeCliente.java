package arquitetura.clean.aplicacao.servicos.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import arquitetura.clean.modelo.entidades.cliente.Cliente;

import java.util.Objects;

public final class ServicoDeAlteracaoDeCliente implements CasoDeUsoDeAlteracaoDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeAlteracaoDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public ClienteDeResposta alterar(ClienteDeRequisicao clienteDeRequisicao) throws ClienteNaoEncontrado {
        Objects.requireNonNull(clienteDeRequisicao, "O argumento \"clienteDeRequisicao\" não pode ser nulo.");
        Cliente cliente = ClienteDeRequisicao.converterParaModelo(clienteDeRequisicao);
        return repositorioDeClientes.alterar(cliente)
                .map(ClienteDeResposta::converterParaResposta)
                .orElseThrow(() -> new ClienteNaoEncontrado(
                        "Cliente %s não encontrado.".formatted(cliente.getCpfj().formatar())));
    }
}
