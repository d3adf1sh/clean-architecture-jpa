package arquitetura.clean.aplicacao.servicos.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import arquitetura.clean.modelo.valores.CPFJ;

import java.util.Objects;

public final class ServicoDeBuscaDeCliente implements CasoDeUsoDeBuscaDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeBuscaDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public ClienteDeResposta buscar(CPFJ cpfj) throws ClienteNaoEncontrado {
        Objects.requireNonNull(cpfj, "O argumento \"cpfj\" não pode ser nulo.");
        return repositorioDeClientes.buscar(cpfj)
                .map(ClienteDeResposta::converterParaResposta)
                .orElseThrow(() -> new ClienteNaoEncontrado("Cliente %s não encontrado.".formatted(cpfj.formatar())));
    }
}
