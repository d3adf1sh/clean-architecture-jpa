package arquitetura.clean.aplicacao.casosdeuso.cliente;

import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;

public interface CasoDeUsoDeInsercaoDeCliente {
    ClienteDeResposta inserir(ClienteDeRequisicao clienteDeRequisicao) throws ClienteJaCadastrado;
}
