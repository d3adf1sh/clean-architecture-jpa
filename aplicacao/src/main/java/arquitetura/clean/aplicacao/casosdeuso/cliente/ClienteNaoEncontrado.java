package arquitetura.clean.aplicacao.casosdeuso.cliente;

public class ClienteNaoEncontrado extends Exception {
    public ClienteNaoEncontrado() {
    }

    public ClienteNaoEncontrado(String message) {
        super(message);
    }

    public ClienteNaoEncontrado(String message, Throwable cause) {
        super(message, cause);
    }

    public ClienteNaoEncontrado(Throwable cause) {
        super(cause);
    }

    public ClienteNaoEncontrado(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
