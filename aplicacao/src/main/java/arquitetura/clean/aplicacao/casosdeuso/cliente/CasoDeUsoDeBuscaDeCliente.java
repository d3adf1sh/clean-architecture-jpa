package arquitetura.clean.aplicacao.casosdeuso.cliente;

import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import arquitetura.clean.modelo.valores.CPFJ;

public interface CasoDeUsoDeBuscaDeCliente {
    ClienteDeResposta buscar(CPFJ cpfj) throws ClienteNaoEncontrado;
}
