package arquitetura.clean.aplicacao.modelo.resposta.cliente;

import arquitetura.clean.modelo.entidades.cliente.Cliente;

import java.util.List;

public record ClienteDeResposta(String nomeFantasia, String razaoSocial, String cpfj,
        List<ContatoDeResposta> contatos) {
    public static ClienteDeResposta converterParaResposta(Cliente cliente) {
        String cpfj = cliente.getCpfj() != null ? cliente.getCpfj().formatar() : null;
        List<ContatoDeResposta> contatos = cliente.getContatos() != null
                ? cliente.getContatos().stream().map(ContatoDeResposta::converterParaResposta).toList() : null;
        return new ClienteDeResposta(cliente.getNomeFantasia(), cliente.getRazaoSocial(), cpfj, contatos);
    }
}
