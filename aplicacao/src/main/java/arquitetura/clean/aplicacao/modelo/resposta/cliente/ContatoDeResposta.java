package arquitetura.clean.aplicacao.modelo.resposta.cliente;

import arquitetura.clean.modelo.entidades.cliente.Contato;

import java.util.List;

public record ContatoDeResposta(String nome, List<TelefoneDeResposta> telefones) {
    public static ContatoDeResposta converterParaResposta(Contato contato) {
        List<TelefoneDeResposta> telefones = contato.getTelefones() != null
                ? contato.getTelefones().stream().map(TelefoneDeResposta::converterParaResposta).toList() : null;
        return new ContatoDeResposta(contato.getNome(), telefones);
    }
}
