package arquitetura.clean.aplicacao.modelo.requisicao.cliente;

import arquitetura.clean.modelo.entidades.cliente.Contato;
import arquitetura.clean.modelo.entidades.cliente.Telefone;

import java.util.List;

public record ContatoDeRequisicao(String nome, List<TelefoneDeRequisicao> telefones) {
    public static Contato converterParaModelo(ContatoDeRequisicao contatoDeRequisicao) {
        List<Telefone> telefones = contatoDeRequisicao.telefones() != null
                ? contatoDeRequisicao.telefones().stream().map(TelefoneDeRequisicao::converterParaModelo).toList()
                : null;
        return new Contato(contatoDeRequisicao.nome(), telefones);
    }
}
