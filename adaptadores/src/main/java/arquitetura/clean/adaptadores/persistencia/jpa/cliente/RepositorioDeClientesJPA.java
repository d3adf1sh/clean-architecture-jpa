package arquitetura.clean.adaptadores.persistencia.jpa.cliente;

import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.modelo.entidades.cliente.Cliente;
import arquitetura.clean.modelo.valores.CPFJ;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class RepositorioDeClientesJPA implements RepositorioDeClientes {
    private final EntityManagerFactory fabricaDeGerenciadoresDeEntidades;

    public RepositorioDeClientesJPA(EntityManagerFactory fabricaDeGerenciadoresDeEntidades) {
        this.fabricaDeGerenciadoresDeEntidades = fabricaDeGerenciadoresDeEntidades;
    }

    @Override
    public Cliente inserir(Cliente cliente) {
        try (EntityManager gerenciador = fabricaDeGerenciadoresDeEntidades.createEntityManager()) {
            ClienteJPA clienteJPA = ClienteJPA.converterParaJPA(cliente);
            clienteJPA.setDataDeInsercao(LocalDateTime.now());
            gerenciador.getTransaction().begin();
            gerenciador.persist(clienteJPA);
            gerenciador.getTransaction().commit();
            return ClienteJPA.converterParaModelo(clienteJPA);
        }
    }

    @Override
    public Optional<Cliente> alterar(Cliente cliente) {
        try (EntityManager gerenciador = fabricaDeGerenciadoresDeEntidades.createEntityManager()) {
            Optional<ClienteJPA> clienteJPASalvo = carregar(gerenciador, cliente.getCpfj());
            if (clienteJPASalvo.isPresent()) {
                ClienteJPA clienteJPA = ClienteJPA.converterParaJPA(cliente);
                clienteJPA.setIdCliente(clienteJPASalvo.get().getIdCliente());
                clienteJPA.setDataDeAlteracao(LocalDateTime.now());
                gerenciador.getTransaction().begin();
                gerenciador.merge(clienteJPA);
                gerenciador.getTransaction().commit();
                return Optional.of(ClienteJPA.converterParaModelo(clienteJPA));
            } else {
                return Optional.empty();
            }
        }
    }

    @Override
    public Optional<Cliente> excluir(CPFJ cpfj) {
        try (EntityManager gerenciador = fabricaDeGerenciadoresDeEntidades.createEntityManager()) {
            Optional<ClienteJPA> clienteJPA = carregar(gerenciador, cpfj);
            if (clienteJPA.isPresent()) {
                gerenciador.getTransaction().begin();
                gerenciador.remove(clienteJPA.get());
                gerenciador.getTransaction().commit();
                return clienteJPA.map(ClienteJPA::converterParaModelo);
            } else {
                return Optional.empty();
            }
        }
    }

    @Override
    public Optional<Cliente> buscar(CPFJ cpfj) {
        try (EntityManager gerenciador = fabricaDeGerenciadoresDeEntidades.createEntityManager()) {
            Optional<ClienteJPA> clienteJPA = carregar(gerenciador, cpfj);
            return clienteJPA.map(ClienteJPA::converterParaModelo);
        }
    }

    private Optional<ClienteJPA> carregar(EntityManager gerenciador, CPFJ cpfj) {
        return gerenciador.createQuery("from ClienteJPA where cpfj = :cpfj", ClienteJPA.class)
                .setParameter("cpfj", cpfj.valor())
                .getResultList()
                .stream()
                .findFirst();
    }

    @Override
    public List<Cliente> consultar(String nomeFantasia) {
        try (EntityManager gerenciador = fabricaDeGerenciadoresDeEntidades.createEntityManager()) {
            List<ClienteJPA> clientesJPA = gerenciador.createQuery(
                            "from ClienteJPA where nomeFantasia like :nomeFantasia", ClienteJPA.class)
                    .setParameter("nomeFantasia", "%" + nomeFantasia + "%")
                    .getResultList();
            return clientesJPA.stream()
                    .map(ClienteJPA::converterParaModelo)
                    .toList();
        }
    }
}
