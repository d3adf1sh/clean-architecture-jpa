package arquitetura.clean.adaptadores.persistencia.jpa;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.Map;

public final class ProvedorDeFabricasDeGerenciadoresDeEntidades {
    private ProvedorDeFabricasDeGerenciadoresDeEntidades() {
    }

    static public EntityManagerFactory criarFabricaDeGerenciadoresDeEntidades(Map<String, String> parametros) {
        return Persistence.createEntityManagerFactory("arquitetura.clean.adaptadores.persistencia.jpa", parametros);
    }
}