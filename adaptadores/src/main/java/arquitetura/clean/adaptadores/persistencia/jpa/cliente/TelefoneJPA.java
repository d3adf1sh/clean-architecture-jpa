package arquitetura.clean.adaptadores.persistencia.jpa.cliente;

import arquitetura.clean.modelo.entidades.cliente.Telefone;
import arquitetura.clean.modelo.valores.Numero;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Telefone")
public class TelefoneJPA {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTelefone;
    @ManyToOne
    private ContatoJPA contato;
    private String descricao;
    private String numero;

    public static TelefoneJPA converterParaJPA(ContatoJPA contato, Telefone telefone) {
        TelefoneJPA telefoneJPA = new TelefoneJPA();
        telefoneJPA.setContato(contato);
        telefoneJPA.setDescricao(telefone.getDescricao());
        if (telefone.getNumero() != null) {
            telefoneJPA.setNumero(telefone.getNumero().valor());
        }

        return telefoneJPA;
    }

    public static Telefone converterParaModelo(TelefoneJPA telefoneJPA) {
        Numero numero = telefoneJPA.getNumero() != null ? new Numero(telefoneJPA.getNumero()) : null;
        return new Telefone(telefoneJPA.getDescricao(), numero);
    }
}