package arquitetura.clean.adaptadores.persistencia.mongodb.cliente;

import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.modelo.entidades.cliente.Cliente;
import arquitetura.clean.modelo.valores.CPFJ;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.regex;

public class RepositorioDeClientesMongoDB implements RepositorioDeClientes {
    private final String url;
    private final String nomeDoBancoDeDados;

    public RepositorioDeClientesMongoDB(String url, String nomeDoBancoDeDados) {
        this.url = url;
        this.nomeDoBancoDeDados = nomeDoBancoDeDados;
    }

    @Override
    public Cliente inserir(Cliente cliente) {
        try (MongoClient conexao = MongoClients.create(url)) {
            MongoDatabase bancoDeDados = conexao.getDatabase(nomeDoBancoDeDados);
            MongoCollection<Document> colecao = bancoDeDados.getCollection("cliente");
            Document documentoDeCliente = ClienteMongoDB.converterParaDocumento(cliente);
            documentoDeCliente.append("dataDeInsercao", LocalDateTime.now());
            colecao.insertOne(documentoDeCliente);
            return ClienteMongoDB.converterParaModelo(documentoDeCliente);
        }
    }

    @Override
    public Optional<Cliente> alterar(Cliente cliente) {
        try (MongoClient conexao = MongoClients.create(url)) {
            MongoDatabase bancoDeDados = conexao.getDatabase(nomeDoBancoDeDados);
            MongoCollection<Document> colecao = bancoDeDados.getCollection("cliente");
            Optional<Document> documentoDeClienteSalvo = carregar(colecao, cliente.getCpfj());
            if (documentoDeClienteSalvo.isPresent()) {
                Document documentoDeCliente = ClienteMongoDB.converterParaDocumento(cliente);
                documentoDeCliente.append("dataDeInsercao", documentoDeClienteSalvo.get().get("dataDeInsercao"));
                documentoDeCliente.append("dataDeAlteracao", LocalDateTime.now());
                colecao.replaceOne(eq("cpfj", cliente.getCpfj().valor()), documentoDeCliente);
                return Optional.of(ClienteMongoDB.converterParaModelo(documentoDeCliente));
            } else {
                return Optional.empty();
            }
        }
    }

    @Override
    public Optional<Cliente> excluir(CPFJ cpfj) {
        try (MongoClient conexao = MongoClients.create(url)) {
            MongoDatabase bancoDeDados = conexao.getDatabase(nomeDoBancoDeDados);
            MongoCollection<Document> colecao = bancoDeDados.getCollection("cliente");
            Optional<Document> documentoDeCliente = carregar(colecao, cpfj);
            if (documentoDeCliente.isPresent()) {
                colecao.deleteOne(eq("cpfj", cpfj.valor()));
                return documentoDeCliente.map(ClienteMongoDB::converterParaModelo);
            } else {
                return Optional.empty();
            }
        }
    }

    @Override
    public Optional<Cliente> buscar(CPFJ cpfj) {
        try (MongoClient conexao = MongoClients.create(url)) {
            MongoDatabase bancoDeDados = conexao.getDatabase(nomeDoBancoDeDados);
            MongoCollection<Document> colecao = bancoDeDados.getCollection("cliente");
            Optional<Document> documentoDeCliente = carregar(colecao, cpfj);
            return documentoDeCliente.map(ClienteMongoDB::converterParaModelo);
        }
    }

    private Optional<Document> carregar(MongoCollection<Document> colecao, CPFJ cpfj) {
        Document documentoDeCliente = colecao.find(eq("cpfj", cpfj.valor()))
                .first();
        return Optional.ofNullable(documentoDeCliente);
    }

    @Override
    public List<Cliente> consultar(String nomeFantasia) {
        try (MongoClient conexao = MongoClients.create(url)) {
            MongoDatabase bancoDeDados = conexao.getDatabase(nomeDoBancoDeDados);
            MongoCollection<Document> colecao = bancoDeDados.getCollection("cliente");
            List<Document> documentoDeClientes = new ArrayList<>();
            colecao.find(regex("nomeFantasia", nomeFantasia))
                    .forEach(documentoDeClientes::add);
            return documentoDeClientes.stream()
                    .map(ClienteMongoDB::converterParaModelo)
                    .toList();
        }
    }
}
