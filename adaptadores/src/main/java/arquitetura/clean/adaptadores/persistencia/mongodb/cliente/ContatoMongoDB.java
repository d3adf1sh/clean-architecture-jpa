package arquitetura.clean.adaptadores.persistencia.mongodb.cliente;

import arquitetura.clean.modelo.entidades.cliente.Contato;
import arquitetura.clean.modelo.entidades.cliente.Telefone;
import org.bson.Document;

import java.util.List;

public final class ContatoMongoDB {
    public static Document converterParaDocumento(Contato contato) {
        Document documentoDeContato = new Document();
        documentoDeContato.append("nome", contato.getNome());
        if (contato.getTelefones() != null) {
            documentoDeContato.append("telefones",
                    contato.getTelefones().stream().map(TelefoneMongoDB::converterParaDocumento).toList());
        }

        return documentoDeContato;
    }

    public static Contato converterParaModelo(Document documentoDeContato) {
        List<Telefone> telefones = documentoDeContato.containsKey("telefones")
                ? documentoDeContato.getList("telefones", Document.class).stream().map(TelefoneMongoDB::converterParaModelo).toList()
                : null;
        return new Contato(documentoDeContato.getString("nome"), telefones);
    }
}