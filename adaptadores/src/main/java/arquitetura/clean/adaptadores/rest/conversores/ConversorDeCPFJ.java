package arquitetura.clean.adaptadores.rest.conversores;

import arquitetura.clean.modelo.valores.CPFJ;
import jakarta.ws.rs.core.Response;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

public final class ConversorDeCPFJ {
    private ConversorDeCPFJ() {
    }

    public static CPFJ de(String cpfj) {
        if (cpfj == null) {
            throw gerarErro(Response.Status.BAD_REQUEST, "CNPJ/CPF não informado.");
        }

        try {
            return CPFJ.de(cpfj);
        } catch (IllegalArgumentException failure) {
            throw gerarErro(Response.Status.BAD_REQUEST, "CNPJ/CPF inválido.");
        }
    }
}
