package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@Path("/cliente")
public class ControladorDeAlteracaoDeCliente {
    private final CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente;

    public ControladorDeAlteracaoDeCliente(CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente) {
        this.casoDeUsoDeAlteracaoDeCliente = casoDeUsoDeAlteracaoDeCliente;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ClienteDeResposta alterar(ClienteDeRequisicao clienteDeRequisicao) {
        try {
            return casoDeUsoDeAlteracaoDeCliente.alterar(clienteDeRequisicao);
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(Response.Status.BAD_REQUEST, failure.getMessage());
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(Response.Status.NOT_FOUND, failure.getMessage());
        }
    }
}
