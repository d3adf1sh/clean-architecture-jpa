package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.adaptadores.rest.conversores.ConversorDeCPFJ;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@Path("/cliente")
public class ControladorDeExclusaoDeCliente {
    private final CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente;

    public ControladorDeExclusaoDeCliente(CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente) {
        this.casoDeUsoDeExclusaoDeCliente = casoDeUsoDeExclusaoDeCliente;
    }

    @DELETE
    @Path("/{cpfj}")
    @Produces(MediaType.APPLICATION_JSON)
    public ClienteDeResposta excluir(@PathParam("cpfj") String cpfj) {
        try {
            return casoDeUsoDeExclusaoDeCliente.excluir(ConversorDeCPFJ.de(cpfj));
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(Response.Status.NOT_FOUND, failure.getMessage());
        }
    }
}
