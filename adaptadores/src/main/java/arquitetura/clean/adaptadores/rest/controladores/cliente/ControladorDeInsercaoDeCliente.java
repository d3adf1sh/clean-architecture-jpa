package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteJaCadastrado;
import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;
import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarResposta;

@Path("/cliente")
public class ControladorDeInsercaoDeCliente {
    private final CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente;

    public ControladorDeInsercaoDeCliente(CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente) {
        this.casoDeUsoDeInsercaoDeCliente = casoDeUsoDeInsercaoDeCliente;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inserir(ClienteDeRequisicao clienteDeRequisicao) {
        try {
            return gerarResposta(Response.Status.CREATED, casoDeUsoDeInsercaoDeCliente.inserir(clienteDeRequisicao));
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(Response.Status.BAD_REQUEST, failure.getMessage());
        } catch (ClienteJaCadastrado failure) {
            throw gerarErro(Response.Status.CONFLICT, failure.getMessage());
        }
    }
}
