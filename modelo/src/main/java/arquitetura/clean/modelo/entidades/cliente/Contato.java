package arquitetura.clean.modelo.entidades.cliente;

import lombok.Getter;

import java.util.List;
import java.util.Objects;

@Getter
public final class Contato {
    private final String nome;
    private final List<Telefone> telefones;

    public Contato(String nome, List<Telefone> telefones) {
        Objects.requireNonNull(nome, "O argumento \"nome\" não pode ser nulo.");
        if (nome.isEmpty()) {
            throw new IllegalArgumentException("O argumento \"nome\" não pode ser vazio.");
        }

        Objects.requireNonNull(telefones, "O argumento \"telefones\" não pode ser nulo.");
        if (telefones.isEmpty()) {
            throw new IllegalArgumentException("O argumento \"telefones\" não pode ser vazio.");
        }

        this.nome = nome;
        this.telefones = telefones;
    }
}
