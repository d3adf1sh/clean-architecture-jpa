package arquitetura.clean.modelo.valores;

import java.util.Objects;

public record Numero(String valor) {
    public Numero {
        Objects.requireNonNull(valor, "O argumento \"valor\" não pode ser nulo.");
        if (valor.length() != 10 && valor.length() != 11) {
            throw new IllegalArgumentException("O argumento \"valor\" deve ter 10 ou 11 dígitos.");
        }
    }

    public String formatar() {
        return valor.replaceAll("(\\d{2})(\\d{%d})(\\d{4})".formatted(valor.length() == 10 ? 4 : 5), "($1) $2-$3");
    }

    public static Numero de(String telefone) {
        Objects.requireNonNull(telefone, "O argumento \"telefone\" não pode ser nulo.");
        return new Numero(telefone.chars()
                .filter(Character::isDigit)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString());
    }
}
