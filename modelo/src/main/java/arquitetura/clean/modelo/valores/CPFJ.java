package arquitetura.clean.modelo.valores;

import arquitetura.clean.modelo.validadores.DigitoVerificador;

import java.util.Objects;

public record CPFJ(String valor) {
    public CPFJ {
        Objects.requireNonNull(valor, "O argumento \"valor\" não pode ser nulo.");
        if (valor.length() != 11 && valor.length() != 14) {
            throw new IllegalArgumentException("O argumento \"valor\" deve ter 11 ou 14 dígitos.");
        }

        if (valor.chars().distinct().count() == 1) {
            throw new IllegalArgumentException("O argumento \"valor\" não pode ter todos os dígitos iguais.");
        }

        DigitoVerificador digitoVerificador;
        if (valor.length() == 11) {
            digitoVerificador = new DigitoVerificador(new int[]{11, 10, 9, 8, 7, 6, 5, 4, 3, 2});
        } else {
            digitoVerificador = new DigitoVerificador(new int[]{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2});
        }

        if (!digitoVerificador.valido(valor)) {
            throw new IllegalArgumentException("O argumento \"valor\" deve ter dígito verificador válido.");
        }
    }

    public String formatar() {
        if (valor.length() == 11) {
            return valor.replaceAll("(\\d{3})(\\d{3})(\\d{3})(\\d{2})", "$1.$2.$3-$4");
        } else {
            return valor.replaceAll("(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})", "$1.$2.$3/$4-$5");
        }
    }

    public static CPFJ de(String cpfj) {
        Objects.requireNonNull(cpfj, "O argumento \"cpfj\" não pode ser nulo.");
        return new CPFJ(cpfj.chars()
                .filter(Character::isDigit)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString());
    }
}
