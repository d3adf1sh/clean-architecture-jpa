# Arquitetura clean em Java com JPA

Este projeto demonstra a implementação de um cadastro de clientes e seus respectivos contatos e telefones através dos
princípios da arquitetura clean.

O objetivo é disponibilizar uma API REST para cadastrar, alterar, excluir, buscar e consultar clientes em bancos de
dados Postgres, MySQL e MongoDB.

## Sobre a arquitetura clean

A arquitetura clean é um padrão de estruturação de aplicação que prevê que:

- A aplicação deve ser igualmente controlável por usuários, outras aplicações ou testes automatizados. Para as regras de
  negócio, não há diferença se a aplicação é utilizada por um usuário através de uma UI ou por uma API REST;
- As regras de negócio devem ser implementadas de forma isolada independente de questões de infraestrutura como bancos
  de dados, frameworks ou outras aplicações. Não faz diferença se os dados são armazenados em um banco relacional, em um
  banco NoSQL ou em uma aplicação externa;
- A troca de componentes de infraestrutura como servidores de bancos de dados e frameworks ou até mesmo a adaptação à
  novas UIs deve ser possível sem ajustes nas regras de negócio.

O isolamento das regras de negócio do mundo externo é atingido através de interfaces que funcionam de forma semelhante
as portas da arquitetura hexagonal:

![Arquitetura clean](docs/arquitetura.jpg)

Os casos de uso e as entidades compõem o *core* da arquitetura. Os casos de uso contém as regras de negócio e servem
para orquestrar as entidades e o fluxo de dados entre elas. Junto com os casos de uso ficam as interfaces que são
responsáveis por conectá-los à UIs, APIs REST, banco de dados, etc.

A conexão com os componentes de infraestrutura é feita através dos adaptadores. Os adaptadores definem, por exemplo,
controladores de APIs REST que disparam ações no *core* através dos casos de uso. Do outro lado, os gateways podem ser
são acionados pelos casos de uso para buscar ou gravar informações em bancos de dados.

## Requisitos

- Linux, Java e Maven;
- `curl` e [jq](https://jqlang.github.io/jq) para execução de APIs.

## Organização

A aplicação está estruturada com os seguintes módulos:

```
clean-architecture-jpa
├── adaptadores
├── aplicacao
├── bootstrap
└── modelo
```

- **modelo**: Contém as classes que representam o cadastro de clientes. Foram criadas classes adicionais para os
  atributos que contém algum tipo de formatação e/ou regra;
- **aplicacao**: Contém os casos de uso, as interfaces para implementação dos adaptadoresa e os serviços que implementam
  os casos de uso. Junto com o modelo, forma o *core* da aplicação;
- **adaptadores**: Contém os adaptadores REST e de persistência;
- **bootstrap**: Contém a classe principal da aplicação e é responsável por inicializá-la.

## Execução

Compilar:

```bash
mvn package
```

Criar o arquivo `config.properties` com as configurações da aplicação:

```properties
# Persistência
persistencia=mysql|postgres|mongodb
## MySQL
persistencia.mysql.servidor=servidor
persistencia.mysql.porta=porta
persistencia.mysql.bancoDeDados=bancoDeDados
persistencia.mysql.usuario=usuario
persistencia.mysql.senha=senha
## Postgres
persistencia.postgres.servidor=servidor
persistencia.postgres.porta=porta
persistencia.postgres.bancoDeDados=bancoDeDados
persistencia.postgres.usuario=usuario
persistencia.postgres.senha=senha
## MongoDB
persistencia.mongodb.servidor=servidor
persistencia.mongodb.porta=porta
persistencia.mongodb.bancoDeDados=bancoDeDados
persistencia.mongodb.usuario=usuario
persistencia.mongodb.senha=senha
```

Executar a aplicação:

```bash
java -jar "-DarquivoDePropriedades=config.properties" bootstrap/target/bootstrap-1.0-SNAPSHOT.jar
```

## Exemplos

Seguem as chamadas da API REST via linha de comando com `curl` e `jq`.

Inserir cliente:

```bash
curl --request POST \
  --url http://localhost:8080/cliente \
  --header 'Content-Type: application/json' \
  --data '{
    "nomeFantasia": "Rafael C. Luiz",
    "razaoSocial": "Rafael C. Luiz",
    "cpfj": "88614664001",
    "contatos": [
      {
        "nome": "Contato 1",
        "telefones": [
          {
            "descricao": "1",
            "numero": "19989892121"
          }
        ]
      },
      {
        "nome": "Contato 2",
        "telefones": [
          {
            "descricao": "1",
            "numero": "19989892121"
          }
        ]
      }
    ]
  }' | jq
```

Alterar cliente:

```bash
curl --request PUT \
  --url http://localhost:8080/cliente \
  --header 'Content-Type: application/json' \
  --data '{
    "nomeFantasia": "Rafael C. Luiz",
    "razaoSocial": "Rafael C. Luiz",
    "cpfj": "88614664001",
    "contatos": [
      {
        "nome": "Contato 1",
        "telefones": [
          {
            "descricao": "1",
            "numero": "19989892121"
          }
        ]
      },
      {
        "nome": "Contato 2",
        "telefones": [
          {
            "descricao": "1",
            "numero": "19989892121"
          }
        ]
      }
    ]
  }' | jq
```

Excluir cliente:

```bash
curl --request DELETE --url http://localhost:8080/cliente/88614664001 | jq
```

Buscar cliente:

```bash
curl --request GET --url http://localhost:8080/cliente/88614664001 | jq
```

Consultar clientes:

```bash
curl --request GET --url 'http://localhost:8080/cliente?nomeFantasia=Rafael' | jq
```