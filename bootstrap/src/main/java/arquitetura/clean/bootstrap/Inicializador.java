package arquitetura.clean.bootstrap;

import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Properties;

public class Inicializador {
    public static void main(String[] args) {
        Inicializador inicializador = new Inicializador();
        inicializador.carregarPropriedades();
        inicializador.iniciarServidor();
    }

    private void carregarPropriedades() {
        String nomeDoArquivo = System.getProperty("arquivoDePropriedades");
        if (nomeDoArquivo == null) {
            File jar = new File(Inicializador.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            nomeDoArquivo = Path.of(jar.getParent(), "config.properties").toString();
        }

        carregarPropriedades(nomeDoArquivo);
    }

    private void carregarPropriedades(String nomeDoArquivo) {
        File arquivo = new File(nomeDoArquivo);
        if (arquivo.exists() && arquivo.isFile()) {
            try (InputStream streamDoArquivo = new FileInputStream(arquivo)) {
                Properties propriedades = new Properties();
                propriedades.load(streamDoArquivo);
                for (String nome : propriedades.stringPropertyNames()) {
                    String valor = propriedades.getProperty(nome);
                    if (valor != null && !valor.isEmpty()) {
                        System.setProperty(nome, valor);
                    }
                }
            } catch (IOException failure) {
                failure.printStackTrace();
            }
        }
    }

    private void iniciarServidor() {
        UndertowJaxrsServer servidor = new UndertowJaxrsServer();
        servidor.setPort(8080);
        servidor.start();
        servidor.deploy(AplicacaoComJPA.class);
    }
}